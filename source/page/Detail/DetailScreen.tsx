import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useRoute} from '@react-navigation/native';
import {fetchGetContactsRequest} from '../../redux/action/getContact/GetContactAction';
import {RootState} from '../../redux/reducer';
import {styles} from './DetailScreen.Style';
import DetailPhoto from '../../component/DetailPhoto/DetailPhoto';

interface NavigationProps {
  navigation: any;
}

const DetailScreen: React.FC<NavigationProps> = ({navigation}) => {
  const dispatch = useDispatch();
  const getcontacts = useSelector(
    (state: RootState) => state.getcontacts.getcontacts,
  );
  const loading = useSelector((state: RootState) => state.getcontacts.loading);
  const error = useSelector((state: RootState) => state.getcontacts.error);
  const route = useRoute();
  const id = route.params.id;

  useEffect(() => {
    dispatch(fetchGetContactsRequest(id));
  }, [dispatch, id]);

  if (loading) {
    return (
      <View style={styles.backgroundIdle}>
        <Text style={styles.textIdle}>Loading...</Text>
      </View>
    );
  }

  if (error) {
    return (
      <View style={styles.backgroundIdle}>
        <Text style={styles.textIdle}>Error: {error}</Text>
      </View>
    );
  }

  return (
    <View style={styles.background}>
      <DetailPhoto navigation={navigation.goBack} image={getcontacts.photo} />
      <View style={{width: '90%', marginHorizontal: 20, marginVertical: 60}}>
        <Text style={styles.topText}>First Name</Text>
        <Text style={styles.botText}>{getcontacts.firstName}</Text>
        <Text style={styles.topText}>Last Name</Text>
        <Text style={styles.botText}>{getcontacts.lastName}</Text>
        <Text style={styles.topText}>Age</Text>
        <Text style={styles.botText}>{getcontacts.age}</Text>
      </View>
    </View>
  );
};

export default DetailScreen;
