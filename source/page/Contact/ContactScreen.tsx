import React, {useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {fetchGetAllContactsRequest} from '../../redux/action/getAllContact/GetAllContactAction';
import {RootState} from '../../redux/reducer';
import Header from '../../component/Header/Header';
import {styles} from './ContactScreen.Style';
import user from '../../asset/icon/user.png';

interface NavigationProps {
  navigation: any;
}

const ContactScreen: React.FC<NavigationProps> = ({navigation}) => {
  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  }, []);

  const dispatch = useDispatch();
  const getallcontacts = useSelector(
    (state: RootState) => state.getallcontacts.getallcontacts,
  );
  const loading = useSelector(
    (state: RootState) => state.getallcontacts.loading,
  );
  const error = useSelector((state: RootState) => state.getallcontacts.error);

  useEffect(() => {
    dispatch(fetchGetAllContactsRequest());
  }, []);

  function isValidUrl(url: string): boolean {
    // Regular expression to match URLs
    const urlRegex =
      /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-._~:/?#[\]@!\$&'()*+,;=]+$/;

    // Test the URL against the regex
    return urlRegex.test(url);
  }

  if (loading) {
    return (
      <View style={styles.backgroundIdle}>
        <Text style={styles.textIdle}>Loading...</Text>
      </View>
    );
  }

  if (error) {
    return (
      <View style={styles.backgroundIdle}>
        <Text style={styles.textIdle}>Error: {error}</Text>
      </View>
    );
  }

  return (
    <View style={styles.background}>
      <Header />
      <FlatList
        style={{...styles.background, marginTop: 15}}
        data={getallcontacts}
        renderItem={({item}) => (
          <TouchableOpacity
            style={styles.flatlist}
            onPress={() => {
              navigation.navigate('DetailScreen', {
                id: item.id,
              });
            }}>
            {isValidUrl(item.photo) === true ? (
              <Image source={{uri: item.photo}} style={styles.photo} />
            ) : (
              <View style={styles.noPhoto}>
                <Image source={user} style={styles.userPhoto} />
              </View>
            )}
            <Text style={styles.textFirstName}>{item.firstName}</Text>
            <Text style={styles.textLastName}>{item.lastName}</Text>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id.toString()}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      />
    </View>
  );
};

export default ContactScreen;
