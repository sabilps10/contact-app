import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  backgroundIdle: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textIdle: {
    fontSize: 25,
    color: '#801256',
  },
  background: {
    flex: 1,
    backgroundColor: '#fff',
  },
  flatlist: {
    marginTop: '5%',
    marginHorizontal: 20,
    flexDirection: 'row',
    width: '90%',
    alignItems: 'center',
  },
  photo:{
    width: 50, 
    height: 50,
    borderRadius: 30,
  },
  noPhoto: {
    width: 50, 
    height: 50,
    borderRadius: 30,
    backgroundColor: '#bfbfbf',
    justifyContent: 'center',
    alignItems: 'center',
  },
  userPhoto: {
    width: 30, 
    height: 30,
  },
  textFirstName: {
    color: '#801256',
    fontSize: 15,
    fontWeight: '300',
    marginLeft: 15,
  },
  textLastName: {
    color: '#801256',
    fontSize: 15,
    fontWeight: '300',
    marginLeft: 4,
  },
});
