import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  backgroundIdle: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textIdle: {
    fontSize: 25,
    color: '#801256',
  },
  background: {
    flex: 1,
    backgroundColor: '#fff',
  },
  topText: {
    fontSize: 25,
    color: '#801256',
    marginTop: 10,
  },
  botText: {
    fontSize: 15,
    color: '#D19A2E',
  },
});
