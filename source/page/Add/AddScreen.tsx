import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useRoute} from '@react-navigation/native';
import {fetchGetContactsRequest} from '../../redux/action/getContact/GetContactAction';
import {RootState} from '../../redux/reducer';
import {styles} from './AddScreen.Style';
import DetailPhoto from '../../component/DetailPhoto/DetailPhoto';

interface NavigationProps {
  navigation: any;
}

const AddScreen: React.FC<NavigationProps> = ({navigation}) => {
  const dispatch = useDispatch();

  return (
    <View style={styles.background}>
      {/* <DetailPhoto navigation={navigation.goBack} image={getcontacts.photo} /> */}
      <View style={{width: '90%', marginHorizontal: 20, marginVertical: 60}}>
        <Text style={styles.topText}>First Name</Text>
        <Text style={styles.botText}></Text>
        <Text style={styles.topText}>Last Name</Text>
        <Text style={styles.botText}></Text>
        <Text style={styles.topText}>Age</Text>
        <Text style={styles.botText}></Text>
      </View>
    </View>
  );
};

export default AddScreen;
