import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    height: '50%',
    backgroundColor: '#801256',
  },
  back: {
    width: 15,
    height: 15,
    marginHorizontal: 15,
    marginVertical: 20,
  },
  picContainer: {
    width: '90%',
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoContainer: {
    width: 225,
    height: 225,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: '48%',
  },
  photo: {
    width: 200,
    height: 200,
    borderRadius: 100,
  },
  noPhotoContainer: {
    width: 225,
    height: 225,
    backgroundColor: '#D19A2E',
    borderRadius: 112.5,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: '48%',
  },
  noPhoto: {
    width: 200,
    height: 200,
  },
});
