import {View, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {styles} from './DetailPhoto.Style';
import userIcon from '../../asset/icon/user.png';
import backIcon from '../../asset/icon/back.png';

interface ParentComponentProps {
  image: any;
  navigation: any;
}

const DetailPhoto: React.FC<ParentComponentProps> = ({image, navigation}) => {
  function isValidUrl(url: string): boolean {
    // Regular expression to match URLs
    const urlRegex =
      /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-._~:/?#[\]@!\$&'()*+,;=]+$/;

    // Test the URL against the regex
    return urlRegex.test(url);
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={navigation}>
        <Image source={backIcon} style={styles.back} />
      </TouchableOpacity>
      <View style={styles.picContainer}>
        {isValidUrl(image) === true ? (
          <View style={styles.photoContainer}>
            <Image source={{uri: image}} style={styles.photo} />
          </View>
        ) : (
          <View style={styles.noPhotoContainer}>
            <Image source={userIcon} style={styles.noPhoto} />
          </View>
        )}
      </View>
    </View>
  );
};

export default DetailPhoto;
