import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {styles} from './Header.Style';
import plus from '../../asset/icon/plus.png';

const Header: React.FC = () => {
  return (
    <View style={styles.header}>
      <Text style={styles.textContact}>Contacts</Text>
      <TouchableOpacity style={styles.plusButton}>
        <Image source={plus} style={styles.plusIcon} />
      </TouchableOpacity>
    </View>
  );
};

export default Header;
