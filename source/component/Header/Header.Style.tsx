import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  header: {
    marginTop: '5%',
    marginHorizontal: 20,
    flexDirection: 'row',
    width: '90%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContact: {
    fontSize: 30,
    letterSpacing: 0.5,
    color: '#801256',
    fontWeight: '700',
  },
  plusButton: {
    width: 25,
    height: 25,
    backgroundColor: '#D19A2E',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  plusIcon: {
    width: 13,
    height: 13,
  },
});
