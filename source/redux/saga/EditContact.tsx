import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import {FETCH_EDIT_CONTACT_REQUEST} from '../action/editContact/EditContactActionType';
import {
  fetchEditContactsSuccess,
  fetchEditContactsFailure,
} from '../action/editContact/EditContactAction';
import {API} from '../../api/Index';

function* fetchEditContacts(payload) {
  try {
    const response = yield axios.put(API + 'contact/', payload.id);
    yield put(fetchEditContactsSuccess(response.data));
  } catch (error) {
    yield put(fetchEditContactsFailure(error.message));
  }
}

export function* editcontacts() {
  yield takeLatest(FETCH_EDIT_CONTACT_REQUEST, fetchEditContacts);
}
