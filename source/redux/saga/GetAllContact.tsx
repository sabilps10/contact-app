import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import {FETCH_GET_ALL_CONTACT_REQUEST} from '../action/getAllContact/GetAllContactActionType';
import {
  fetchGetAllContactsSuccess,
  fetchGetAllContactsFailure,
} from '../action/getAllContact/GetAllContactAction';
import {API} from '../../api/Index';

function* fetchGetAllContacts() {
  try {
    const response = yield axios.get(API + 'contact');
    yield put(fetchGetAllContactsSuccess(response.data.data));
  } catch (error) {
    yield put(fetchGetAllContactsFailure(error.message));
  }
}

export function* getallcontacts() {
  yield takeLatest(FETCH_GET_ALL_CONTACT_REQUEST, fetchGetAllContacts);
}
