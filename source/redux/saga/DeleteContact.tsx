import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import {FETCH_DELETE_CONTACT_REQUEST} from '../action/deleteContact/DeleteContactActionType';
import {
  fetchDeleteContactsSuccess,
  fetchDeleteContactsFailure,
} from '../action/deleteContact/DeleteContactAction';
import {API} from '../../api/Index';

function* fetchDeleteContacts(payload) {
  try {
    const response = yield axios.delete(API + 'contact/' + payload.id);
    yield put(fetchDeleteContactsSuccess(response.data));
  } catch (error) {
    yield put(fetchDeleteContactsFailure(error.message));
  }
}

export function* deletecontacts() {
  yield takeLatest(FETCH_DELETE_CONTACT_REQUEST, fetchDeleteContacts);
}
