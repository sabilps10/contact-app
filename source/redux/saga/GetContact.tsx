import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import {FETCH_GET_CONTACT_REQUEST} from '../action/getContact/GetContactActionType';
import {
  fetchGetContactsSuccess,
  fetchGetContactsFailure,
} from '../action/getContact/GetContactAction';
import {API} from '../../api/Index';

function* fetchGetContacts(payload) {
  try {
    const response = yield axios.get(API + 'contact/' + payload.id);
    yield put(fetchGetContactsSuccess(response.data.data));
  } catch (error) {
    yield put(fetchGetContactsFailure(error.message));
  }
}

export function* getcontacts() {
  yield takeLatest(FETCH_GET_CONTACT_REQUEST, fetchGetContacts);
}
