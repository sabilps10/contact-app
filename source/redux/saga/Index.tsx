import {all} from 'redux-saga/effects';
import {addcontacts} from './AddContact';
import {deletecontacts} from './DeleteContact';
import {editcontacts} from './EditContact';
import {getallcontacts} from './GetAllContact';
import {getcontacts} from './GetContact';

function* rootSaga() {
  yield all([
    // other sagas here
    addcontacts(),
    deletecontacts(),
    editcontacts(),
    getallcontacts(),
    getcontacts(),
  ]);
}

export default rootSaga;
