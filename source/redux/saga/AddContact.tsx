import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import {FETCH_ADD_CONTACT_REQUEST} from '../action/addContact/AddContactActionType';
import {
  fetchAddContactsSuccess,
  fetchAddContactsFailure,
} from '../action/addContact/AddContactAction';
import {API} from '../../api/Index';

function* fetchAddContacts() {
  try {
    const response = yield axios.get(API + 'contact');
    yield put(fetchAddContactsSuccess(response.data));
  } catch (error) {
    yield put(fetchAddContactsFailure(error.message));
  }
}

export function* addcontacts() {
  yield takeLatest(FETCH_ADD_CONTACT_REQUEST, fetchAddContacts);
}
