import {
  FETCH_EDIT_CONTACT_REQUEST,
  FETCH_EDIT_CONTACT_SUCCESS,
  FETCH_EDIT_CONTACT_FAILURE,
} from './EditContactActionType';

export interface EditContact {
  id: any;
  firstName: string;
  lastName: string;
  age: number;
  photo: any;
}

export interface FetchEditContactsRequestAction {
  type: typeof FETCH_EDIT_CONTACT_REQUEST;
  id: any;
}

export interface FetchEditContactsSuccessAction {
  type: typeof FETCH_EDIT_CONTACT_SUCCESS;
  payload: EditContact[];
}

export interface FetchEditContactsFailureAction {
  type: typeof FETCH_EDIT_CONTACT_FAILURE;
  error: string;
}

export type EditContactActionTypes =
  | FetchEditContactsRequestAction
  | FetchEditContactsSuccessAction
  | FetchEditContactsFailureAction;

export const fetchEditContactsRequest = (id: any): EditContactActionTypes => ({
  type: FETCH_EDIT_CONTACT_REQUEST,
  id,
});

export const fetchEditContactsSuccess = (
  editcontacts: EditContact[],
): EditContactActionTypes => ({
  type: FETCH_EDIT_CONTACT_SUCCESS,
  payload: editcontacts,
});

export const fetchEditContactsFailure = (
  error: string,
): EditContactActionTypes => ({
  type: FETCH_EDIT_CONTACT_FAILURE,
  error,
});
