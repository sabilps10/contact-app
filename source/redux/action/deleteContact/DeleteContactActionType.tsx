export const FETCH_DELETE_CONTACT_REQUEST = 'FETCH_DELETE_CONTACT_REQUEST';
export const FETCH_DELETE_CONTACT_SUCCESS = 'FETCH_DELETE_CONTACT_SUCCESS';
export const FETCH_DELETE_CONTACT_FAILURE = 'FETCH_DELETE_CONTACT_FAILURE';
