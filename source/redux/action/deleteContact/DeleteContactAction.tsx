import {
  FETCH_DELETE_CONTACT_REQUEST,
  FETCH_DELETE_CONTACT_SUCCESS,
  FETCH_DELETE_CONTACT_FAILURE,
} from './DeleteContactActionType';

export interface DeleteContact {
  id: any;
  firstName: string;
  lastName: string;
  age: number;
  photo: any;
}

export interface FetchDeleteContactsRequestAction {
  type: typeof FETCH_DELETE_CONTACT_REQUEST;
  id: any;
}

export interface FetchDeleteContactsSuccessAction {
  type: typeof FETCH_DELETE_CONTACT_SUCCESS;
  payload: DeleteContact[];
}

export interface FetchDeleteContactsFailureAction {
  type: typeof FETCH_DELETE_CONTACT_FAILURE;
  error: string;
}

export type DeleteContactActionTypes =
  | FetchDeleteContactsRequestAction
  | FetchDeleteContactsSuccessAction
  | FetchDeleteContactsFailureAction;

export const fetchDeleteContactsRequest = (
  id: any,
): DeleteContactActionTypes => ({
  type: FETCH_DELETE_CONTACT_REQUEST,
  id,
});

export const fetchDeleteContactsSuccess = (
  deletecontacts: DeleteContact[],
): DeleteContactActionTypes => ({
  type: FETCH_DELETE_CONTACT_SUCCESS,
  payload: deletecontacts,
});

export const fetchDeleteContactsFailure = (
  error: string,
): DeleteContactActionTypes => ({
  type: FETCH_DELETE_CONTACT_FAILURE,
  error,
});
