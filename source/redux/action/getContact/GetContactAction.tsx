import {
  FETCH_GET_CONTACT_REQUEST,
  FETCH_GET_CONTACT_SUCCESS,
  FETCH_GET_CONTACT_FAILURE,
} from './GetContactActionType';

export interface GetContact {
  id: any;
  firstName: string;
  lastName: string;
  age: number;
  photo: any;
}

export interface FetchGetContactsRequestAction {
  type: typeof FETCH_GET_CONTACT_REQUEST;
  id: any;
}

export interface FetchGetContactsSuccessAction {
  type: typeof FETCH_GET_CONTACT_SUCCESS;
  payload: GetContact[];
}

export interface FetchGetContactsFailureAction {
  type: typeof FETCH_GET_CONTACT_FAILURE;
  error: string;
}

export type GetContactActionTypes =
  | FetchGetContactsRequestAction
  | FetchGetContactsSuccessAction
  | FetchGetContactsFailureAction;

export const fetchGetContactsRequest = (id: any): GetContactActionTypes => ({
  type: FETCH_GET_CONTACT_REQUEST,
  id,
});

export const fetchGetContactsSuccess = (
  getcontacts: GetContact[],
): GetContactActionTypes => ({
  type: FETCH_GET_CONTACT_SUCCESS,
  payload: getcontacts,
});

export const fetchGetContactsFailure = (
  error: string,
): GetContactActionTypes => ({
  type: FETCH_GET_CONTACT_FAILURE,
  error,
});
