import {
  FETCH_ADD_CONTACT_REQUEST,
  FETCH_ADD_CONTACT_SUCCESS,
  FETCH_ADD_CONTACT_FAILURE,
} from './AddContactActionType';

export interface AddContact {
  id: any;
  firstName: string;
  lastName: string;
  age: number;
  photo: any;
}

export interface FetchAddContactsRequestAction {
  type: typeof FETCH_ADD_CONTACT_REQUEST;
}

export interface FetchAddContactsSuccessAction {
  type: typeof FETCH_ADD_CONTACT_SUCCESS;
  payload: AddContact[];
}

export interface FetchAddContactsFailureAction {
  type: typeof FETCH_ADD_CONTACT_FAILURE;
  error: string;
}

export type AddContactActionTypes =
  | FetchAddContactsRequestAction
  | FetchAddContactsSuccessAction
  | FetchAddContactsFailureAction;

export const fetchAddContactsRequest = (): AddContactActionTypes => ({
  type: FETCH_ADD_CONTACT_REQUEST,
});

export const fetchAddContactsSuccess = (
  addcontacts: AddContact[],
): AddContactActionTypes => ({
  type: FETCH_ADD_CONTACT_SUCCESS,
  payload: addcontacts,
});

export const fetchAddContactsFailure = (
  error: string,
): AddContactActionTypes => ({
  type: FETCH_ADD_CONTACT_FAILURE,
  error,
});
