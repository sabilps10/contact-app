import {
  FETCH_GET_ALL_CONTACT_REQUEST,
  FETCH_GET_ALL_CONTACT_SUCCESS,
  FETCH_GET_ALL_CONTACT_FAILURE,
} from './GetAllContactActionType';

export interface GetAllContact {
  id: any;
  firstName: string;
  lastName: string;
  age: number;
  photo: any;
}

export interface FetchGetAllContactsRequestAction {
  type: typeof FETCH_GET_ALL_CONTACT_REQUEST;
}

export interface FetchGetAllContactsSuccessAction {
  type: typeof FETCH_GET_ALL_CONTACT_SUCCESS;
  payload: GetAllContact[];
}

export interface FetchGetAllContactsFailureAction {
  type: typeof FETCH_GET_ALL_CONTACT_FAILURE;
  error: string;
}

export type GetAllContactActionTypes =
  | FetchGetAllContactsRequestAction
  | FetchGetAllContactsSuccessAction
  | FetchGetAllContactsFailureAction;

export const fetchGetAllContactsRequest = (): GetAllContactActionTypes => ({
  type: FETCH_GET_ALL_CONTACT_REQUEST,
});

export const fetchGetAllContactsSuccess = (
  getallcontacts: GetAllContact[],
): GetAllContactActionTypes => ({
  type: FETCH_GET_ALL_CONTACT_SUCCESS,
  payload: getallcontacts,
});

export const fetchGetAllContactsFailure = (
  error: string,
): GetAllContactActionTypes => ({
  type: FETCH_GET_ALL_CONTACT_FAILURE,
  error,
});
