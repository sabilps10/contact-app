import {
  AddContact,
  AddContactActionTypes,
} from '../action/addContact/AddContactAction';
import {
  FETCH_ADD_CONTACT_REQUEST,
  FETCH_ADD_CONTACT_SUCCESS,
  FETCH_ADD_CONTACT_FAILURE,
} from '../action/addContact/AddContactActionType';

export interface AddContactState {
  addcontacts: AddContact[];
  loading: boolean;
  error: any;
}

const initialState: AddContactState = {
  addcontacts: [],
  loading: false,
  error: null,
};

const addContactReducer = (
  state = initialState,
  action: AddContactActionTypes,
): AddContactState => {
  switch (action.type) {
    case FETCH_ADD_CONTACT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_ADD_CONTACT_SUCCESS:
      return {
        ...state,
        loading: false,
        addcontacts: action.payload,
        error: null,
      };
    case FETCH_ADD_CONTACT_FAILURE:
      return {
        ...state,
        loading: false,
        addcontacts: [],
        error: action.error,
      };
    default:
      return state;
  }
};

export default addContactReducer;
