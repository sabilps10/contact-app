import {
  DeleteContact,
  DeleteContactActionTypes,
} from '../action/deleteContact/DeleteContactAction';
import {
  FETCH_DELETE_CONTACT_REQUEST,
  FETCH_DELETE_CONTACT_SUCCESS,
  FETCH_DELETE_CONTACT_FAILURE,
} from '../action/deleteContact/DeleteContactActionType';

export interface DeleteContactState {
  deletecontacts: DeleteContact[];
  loading: boolean;
  error: any;
}

const initialState: DeleteContactState = {
  deletecontacts: [],
  loading: false,
  error: null,
};

const deleteContactReducer = (
  state = initialState,
  action: DeleteContactActionTypes,
): DeleteContactState => {
  switch (action.type) {
    case FETCH_DELETE_CONTACT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_DELETE_CONTACT_SUCCESS:
      return {
        ...state,
        loading: false,
        deletecontacts: action.payload,
        error: null,
      };
    case FETCH_DELETE_CONTACT_FAILURE:
      return {
        ...state,
        loading: false,
        deletecontacts: [],
        error: action.error,
      };
    default:
      return state;
  }
};

export default deleteContactReducer;
