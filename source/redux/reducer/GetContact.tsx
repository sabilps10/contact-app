import {
  GetContact,
  GetContactActionTypes,
} from '../action/getContact/GetContactAction';
import {
  FETCH_GET_CONTACT_REQUEST,
  FETCH_GET_CONTACT_SUCCESS,
  FETCH_GET_CONTACT_FAILURE,
} from '../action/getContact/GetContactActionType';

export interface GetContactState {
  getcontacts: GetContact[];
  loading: boolean;
  error: any;
}

const initialState: GetContactState = {
  getcontacts: [],
  loading: false,
  error: null,
};

const getContactReducer = (
  state = initialState,
  action: GetContactActionTypes,
): GetContactState => {
  switch (action.type) {
    case FETCH_GET_CONTACT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_GET_CONTACT_SUCCESS:
      return {
        ...state,
        loading: false,
        getcontacts: action.payload,
        error: null,
      };
    case FETCH_GET_CONTACT_FAILURE:
      return {
        ...state,
        loading: false,
        getcontacts: [],
        error: action.error,
      };
    default:
      return state;
  }
};

export default getContactReducer;
