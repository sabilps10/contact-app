import {
  EditContact,
  EditContactActionTypes,
} from '../action/editContact/EditContactAction';
import {
  FETCH_EDIT_CONTACT_REQUEST,
  FETCH_EDIT_CONTACT_SUCCESS,
  FETCH_EDIT_CONTACT_FAILURE,
} from '../action/editContact/EditContactActionType';

export interface EditContactState {
  editcontacts: EditContact[];
  loading: boolean;
  error: any;
}

const initialState: EditContactState = {
  editcontacts: [],
  loading: false,
  error: null,
};

const editContactReducer = (
  state = initialState,
  action: EditContactActionTypes,
): EditContactState => {
  switch (action.type) {
    case FETCH_EDIT_CONTACT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_EDIT_CONTACT_SUCCESS:
      return {
        ...state,
        loading: false,
        editcontacts: action.payload,
        error: null,
      };
    case FETCH_EDIT_CONTACT_FAILURE:
      return {
        ...state,
        loading: false,
        editcontacts: [],
        error: action.error,
      };
    default:
      return state;
  }
};

export default editContactReducer;
