import {combineReducers} from 'redux';
import addContactReducer, {AddContactState} from './AddContact';
import deleteContactReducer, {DeleteContactState} from './DeleteContact';
import editContactReducer, {EditContactState} from './EditContact';
import getAllContactReducer, {GetAllContactState} from './GetAllContact';
import getContactReducer, {GetContactState} from './GetContact';

export interface RootState {
  addcontacts: AddContactState;
  deletecontacts: DeleteContactState;
  editcontacts: EditContactState;
  getallcontacts: GetAllContactState;
  getcontacts: GetContactState;
  loading: boolean;
  error: any;
}

const rootReducer = combineReducers<RootState>({
  addcontacts: addContactReducer,
  editcontacts: editContactReducer,
  deletecontacts: deleteContactReducer,
  getallcontacts: getAllContactReducer,
  getcontacts: getContactReducer,
});

export default rootReducer;
