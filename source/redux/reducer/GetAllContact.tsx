import {
  GetAllContact,
  GetAllContactActionTypes,
} from '../action/getAllContact/GetAllContactAction';
import {
  FETCH_GET_ALL_CONTACT_REQUEST,
  FETCH_GET_ALL_CONTACT_SUCCESS,
  FETCH_GET_ALL_CONTACT_FAILURE,
} from '../action/getAllContact/GetAllContactActionType';

export interface GetAllContactState {
  getallcontacts: GetAllContact[];
  loading: boolean;
  error: any;
}

const initialState: GetAllContactState = {
  getallcontacts: [],
  loading: false,
  error: null,
};

const getAllContactReducer = (
  state = initialState,
  action: GetAllContactActionTypes,
): GetAllContactState => {
  switch (action.type) {
    case FETCH_GET_ALL_CONTACT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_GET_ALL_CONTACT_SUCCESS:
      return {
        ...state,
        loading: false,
        getallcontacts: action.payload.sort((a, b) =>
          a.firstName.localeCompare(b.firstName),
        ),
        error: null,
      };
    case FETCH_GET_ALL_CONTACT_FAILURE:
      return {
        ...state,
        loading: false,
        getallcontacts: [],
        error: action.error,
      };
    default:
      return state;
  }
};

export default getAllContactReducer;
