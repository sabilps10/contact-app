// src/index.tsx

import React from 'react';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';
import App from './App';
import configureStore from './source/store/ConfigureStore';
import {name as appName} from './app.json';

const store = configureStore();

const Main = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => Main);
