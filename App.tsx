// App.js
import React from 'react';
import AppNavigator from './source/navigator/AppNavigator';

const App = () => {
  return <AppNavigator />;
};

export default App;
